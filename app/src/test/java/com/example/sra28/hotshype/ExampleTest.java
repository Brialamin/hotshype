package com.example.sra28.hotshype;

import android.text.TextUtils;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.any;

/**
 * Created by sadams on 2/13/18.
 */

//If testing Static Methods from Android classes, the following needs to be done
    /*
    @RunWith(PowerMockRunner.class)
    @PrepareForTest(AndroidClassWithStaticMethod.class)
    public class ClassUnderTesting {
        @Before
        public void prepareBeforeTests() {
            PowerMockito.mockStatic(AndroidClassWithStaticMethod.class);

            //If only expecting a simple return with an expected arg (Putting "hello world" to be formatted to "Hello World"
            PowerMockito.when(AndroidClassWithStaticMethod.staticMethod(args)).thenAnswer(invocation -> expected);

            //If expecting a more dynamic response, would need to include basic functionality of the method being stubbed
            PowerMockito.when(AndroidClassWithStaticMethod.staticMethod(args)).thenAnswer(invocation -> {
                //Add expected functionality of method here
            });
        }
        .
        .
        .
    }
    */

//Below is a template for setting up a test
    /*
        @Test
        public void testFunctionName_parameterTested_returnValue() throws Exception {
            //variable definitions to pass to the test function defined here
            Object param1 = new Object();
            Object param2 = new Object();

            //actual and expected values defined here
            Object actual = testFunctionName(param1, param2);
            Object expected = new Object("expected");

            //assertEquals
            assertEquals(expected, actual);
        }
        NOTE for formatting function names with numbers
        ------------------
        N - negative
        P - parenthesis
        D - dollar sign
        d - decimal
        c - comma
        ------------------
     */

@RunWith(PowerMockRunner.class)
@PrepareForTest(TextUtils.class)
public class ExampleTest {

    @Before
    public void prepareBeforeTests() {
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty(any(CharSequence.class))).thenAnswer(invocation -> {
            CharSequence a = (CharSequence) invocation.getArguments()[0];
            //This should be !(a != null && a.length() > 0) but for displaying NPE the null check is removed
            return !(a.length() > 0);
        });
    }

    @Test(expected = NullPointerException.class)
    public void exampleUnitTest_parameterNull_returnNPE() {
        String parameter = null;

        boolean actual = TextUtils.isEmpty(parameter);
    }

    @Test
    public void exampleUnitTest_parameterEmpty_returnsTrue() throws Exception {
        String parameter = "";

        boolean actual = TextUtils.isEmpty(parameter);
        boolean expected = true;

        assertEquals(expected, actual);
    }

    @Test
    public void exampleUnitTest_parameterHasText_returnsFalse() throws Exception {
        String parameter = "Hello Unit Test";

        boolean actual = TextUtils.isEmpty(parameter);
        boolean expected = false;

        assertEquals(expected, actual);
    }

}