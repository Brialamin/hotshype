package com.example.sra28.hotshype;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sra28.hotshype.data.model.Conversation;
import com.example.sra28.hotshype.data.model.Role;
import com.example.sra28.hotshype.data.model.User;
import com.example.sra28.hotshype.data.remote.ApiUtils;
import com.example.sra28.hotshype.drawingObjects.ConversationAdapter;
import com.example.sra28.hotshype.drawingObjects.NDSpinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserDetailsActivity extends BaseActivity{

    ListView userView;
    User user;
    int userId = 0;
    boolean userIsInteracting;
    boolean isLoggedInUser = false;

    NDSpinner profileMenu;

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        userIsInteracting = true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_user_details);
        TAG = TAG + "-UserDetailsActivity";
        mService = ApiUtils.getHotsHypeService();

        Button saveProfile = (Button) findViewById(R.id.save_profile_button);
        saveProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveProfile();
            }
        });

        progress.setMessage("Loading player...");

        Intent i = getIntent();
        Log.d(TAG, "Intent: " + i.getExtras().toString());
        userId = i.getIntExtra("user_id", 0);

        if(userId == User.getStoredUserId(settings))
            isLoggedInUser = true;

        manageProfileMenu("general", isLoggedInUser);

        userView = (ListView) findViewById(R.id.user_list);

        profileMenu = (NDSpinner) findViewById(R.id.profileMenu);

        final CustomDarkSpinner profileMenuAdapter = new CustomDarkSpinner(UserDetailsActivity.this, new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.profileMenu))));

        profileMenu.setAdapter(profileMenuAdapter);

        profileMenu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString().toLowerCase();
                if (userIsInteracting) {
                    manageProfileMenu(item, isLoggedInUser);
                    userIsInteracting = false;
                    parent.setSelection(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        if(!isLoggedInUser) {
            profileMenu.setVisibility(View.INVISIBLE);
        }

        getUser();
    }

    public void getUser()
    {
        progress.show();
        Log.d(TAG, "Call URL: " + mService.getUsers().request().url().toString());
        mService.getUsers(userId).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(response.isSuccessful()) {
                    Log.d(TAG, "users loaded from API");
                    manageUserData(response.body());
                }else {
                    progress.dismiss();
                    int statusCode  = response.code();
                    Log.d(TAG, "Unable to load users! Status Code: " + statusCode);
                    // handle request errors depending on status code
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                progress.dismiss();
                Log.d(TAG, "Error communicating with the server.  Error: " + t.toString());
            }
        });
    }

    private void manageUserData(User user)
    {
        this.user = user;
        getSupportActionBar().setTitle(user.getName());
        generalDataTab(user);
        if(isLoggedInUser) {
            messagesDataTab(user);
            manageDataTab();
        }
        else {
            progress.dismiss();
        }
    }

    private void generalDataTab(User user)
    {
        TextView bnet = (TextView) findViewById(R.id.bnet_tag);
        TextView twitch = (TextView) findViewById(R.id.twitch_name);
        bnet.setText(user.getUserData().getBnetTag());

        twitch.setClickable(true);
        twitch.setMovementMethod(LinkMovementMethod.getInstance());
        String text = "<a href='http://twitch.tv/"+user.getUserData().getTwitchName() +"'>" + user.getUserData().getTwitchName() + "</a>";
        twitch.setText(Html.fromHtml(text));

        populateRoles(user);
    }

    private void populateRoles(User user)
    {
        LinearLayout roleLayout = (LinearLayout) findViewById(R.id.roles_general);
        List<Role> roles = user.getRoles();

        for (Role role : roles)
        {
            if (role.getSelected() == 1)
            {
                TextView roleView = new TextView(this);
                roleView.setText(" - " + _Roles[role.getRoleId()-1]);
                roleView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                roleView.setTextColor(getResources().getColor(R.color.inv_text));
                roleView.setTextSize(15);
                roleLayout.addView(roleView);
            }
        }
    }

    private void messagesDataTab(User user)
    {
        mService.getUserConversations(user.getId(), "Bearer "+getApi_key(), "application/json").enqueue(new Callback<List<Conversation>>() {
            @Override
            public void onResponse(Call<List<Conversation>> call, Response<List<Conversation>> response) {
                if(response.isSuccessful()) {
                    Log.d(TAG, "successfully gathered conversations");
                    createConversationView(response.body());
                }else {
                    int statusCode  = response.code();
                    Log.d(TAG, "Unable to get conversations! Status Code: " + statusCode);
                    // handle request errors depending on status code
                }
            }

            @Override
            public void onFailure(Call<List<Conversation>> call, Throwable t) {
                Log.d(TAG, "Error communicating with the server.  Error: " + t.toString());
            }
        });
    }

    private void manageDataTab()
    {
        EditText bnet = (EditText) findViewById(R.id.manage_bnet);
        EditText twitch = (EditText) findViewById(R.id.manage_twitch);
        CheckBox lft = (CheckBox) findViewById(R.id.lft);
        LinearLayout roleLayout = (LinearLayout) findViewById(R.id.manage_roles);

        if(user.getUserData().getRecruitable() == 1)
            lft.setChecked(true);

        List<Role> roles = user.getRoles();

        bnet.setText(user.getUserData().getBnetTag());
        twitch.setText(user.getUserData().getTwitchName());

        bnet.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                user.getUserData().setBnetTag(s.toString());
            }
        });

        twitch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                user.getUserData().setTwitchName(s.toString());
            }
        });

        lft.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked())
                {
                    user.getUserData().setRecruitable(1);
                    Log.d(TAG, "Checked: LFT");
                }
                else
                {
                    user.getUserData().setRecruitable(0);
                    Log.d(TAG, "Unchecked: LFT");
                }
            }
        });

        for (final Role role : roles)
        {
            CheckBox roleView = new CheckBox(this);
            roleView.setText(" " + _Roles[role.getRoleId()-1]);
            roleView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            roleView.setTextColor(getResources().getColor(R.color.inv_text));
            roleView.setTextSize(15);

            if (role.getSelected() == 1)
                roleView.setChecked(true);

            roleView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(buttonView.isChecked())
                    {
                        role.setSelected(1);
                        Log.d(TAG, "Checked: " + _Roles[role.getRoleId()-1]);
                    }
                    else
                    {
                        role.setSelected(0);
                        Log.d(TAG, "Unchecked: " + _Roles[role.getRoleId()-1]);
                    }
                }
            });
            roleLayout.addView(roleView);
        }
    }

    private void manageProfileMenu(String menuSelection, Boolean loggedInUser)
    {
        hideMenuSelections();
        switch(menuSelection)
        {
            case "general":
                LinearLayout general = (LinearLayout) findViewById(R.id.general);
                if(!loggedInUser) {
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) general.getLayoutParams();
                    params.addRule(RelativeLayout.BELOW, 0);
                }
                general.setVisibility(View.VISIBLE);
                break;
            case "messages":
                ListView messages = (ListView) findViewById(R.id.messages);
                messages.setVisibility(View.VISIBLE);
                break;
            case "manage":
                ScrollView manage = (ScrollView) findViewById(R.id.manage);
                manage.setVisibility(View.VISIBLE);
                break;
            default:
                TextView comingSoon = (TextView) findViewById(R.id.coming_soon);
                comingSoon.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void hideMenuSelections()
    {
        LinearLayout general = (LinearLayout) findViewById(R.id.general);
        ScrollView manage = (ScrollView) findViewById(R.id.manage);
        ListView messages = (ListView) findViewById(R.id.messages);
        TextView soon = (TextView) findViewById(R.id.coming_soon);

        general.setVisibility(View.GONE);
        manage.setVisibility(View.GONE);
        messages.setVisibility(View.GONE);
        soon.setVisibility(View.GONE);
    }

    private void saveProfile()
    {
        progress.setMessage("Saving player...");
        List<Role> roles = user.getRoles();
        Log.d(TAG, roles.toString());

        progress.show();
        mService.saveUser(user, user.getId(), "Bearer "+getApi_key(), "application/json").enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()) {
                    Log.d(TAG, "user saved");
                }else {
                    progress.dismiss();
                    int statusCode  = response.code();
                    Log.d(TAG, "Unable to save user! Status Code: " + statusCode);
                    // handle request errors depending on status code
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                progress.dismiss();
                Log.d(TAG, "Error communicating with the server.  Error: " + t.toString());
            }
        });
        for(Role role : roles)
        {
            mService.saveRoles(role, role.getId(), "Bearer "+getApi_key(), "application/json").enqueue(new Callback<    String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if(response.isSuccessful()) {
                        Log.d(TAG, "roles saved");

                    }else {
                        progress.dismiss();
                        int statusCode  = response.code();
                        Log.d(TAG, "Unable to save roles! Status Code: " + statusCode);
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    progress.dismiss();
                    Log.d(TAG, "Error communicating with the server.  Error: " + t.toString());
                }
            });
        }
        progress.dismiss();
        Toast.makeText(this.getApplicationContext(), "Profile Updated", Toast.LENGTH_SHORT).show();

    }

    private void createConversationView(List<Conversation> conversations)
    {
        for(Conversation conversation : conversations)
        {
            conversation.getUsers(mService);
            conversation.getLastMessage(mService);
        }
        final ListView convoView = (ListView) findViewById(R.id.messages);

        ConversationAdapter mAdapter = new ConversationAdapter(this, R.layout.list_view_text, conversations);

        convoView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int convo_id = ((Conversation) convoView.getAdapter().getItem(position)).getId();
                Intent i = new Intent(UserDetailsActivity.this, ConversationDetailsActivity.class);
                i.putExtra("conversation_id", convo_id);
                startActivity(i);
            }
        });

        convoView.setAdapter(mAdapter);

        progress.dismiss();
    }

}
