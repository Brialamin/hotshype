package com.example.sra28.hotshype.data.remote;

/**
 * Created by sra28 on 7/14/2017.
 */

public class ApiUtils {

    public static final String BASE_URL = "http://hotshype.duckdns.org/";

    public static HotsHypeService getHotsHypeService() {
        return RetrofitClient.getClient(BASE_URL).create(HotsHypeService.class);
    }
}
