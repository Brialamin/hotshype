package com.example.sra28.hotshype.data.remote;

import com.example.sra28.hotshype.data.model.*;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HEAD;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by sra28 on 7/14/2017.
 */

public interface HotsHypeService {

    @GET("/api/users")
    Call<List<User>> getUsers();

    @GET("/api/users/{id}")
    Call<User> getUsers(@Path("id") int id);

    @GET("/api/users/{email}")
    Call<User> getUsers(@Path("email") String email);

    @FormUrlEncoded
    @POST("/oauth/token")
    Call<ApiTokenRequest> login(@Field("grant_type") String grant_type,
                     @Field("client_id") String client_id,
                     @Field("client_secret") String client_secret,
                     @Field("username") String username,
                     @Field("password") String password,
                     @Field("scope") String scope);

    @FormUrlEncoded
    @POST("/api/users/register")
    Call<User> register(@Field("battlenet-tag") String battlenet_tag,
                                @Field("twitch-name") String twitch_name,
                                @Field("name") String name,
                                @Field("email") String email,
                                @Field("password") String password);

    @POST("/api/roles/{id}/update")
    Call<String> saveRoles(@Body Role role,
                           @Path("id") int id,
                           @Header("Authorization") String auth,
                           @Header("Accept") String accept);

    @POST("/api/users/{id}/update")
    Call<String> saveUser(@Body User user,
                          @Path("id") int id,
                          @Header("Authorization") String auth,
                          @Header("Accept") String accept);

    @GET("/api/users/{id}/messages")
    Call<List<Message>> getUserLastMessages(@Path("id") int id,
                                    @Header("Authorization") String auth,
                                    @Header("Accept") String accept);

    @GET("/api/conversation/{id}")
    Call<List<Message>> getConversationMessages(@Path("id") int id,
                                                @Header("Authorization") String auth,
                                                @Header("Accept") String accept);

    @POST("/api/messages/create")
    Call<String> createMessage(@Body Message message,
                               @Header("Authorization") String auth,
                               @Header("Accept") String accept);

    @GET("/api/conversation/{id}/users")
    Call<List<User>> getConversationUsers(@Path("id") int id);

    @GET("/api/conversation/{id}/lastmessage")
    Call<Message> getConversationLastMessage(@Path("id") int id);

    @GET("/api/users/{id}/conversations")
    Call<List<Conversation>> getUserConversations(@Path("id") int id,
                                                  @Header("Authorization") String auth,
                                                  @Header("Accept") String accept);

    @GET("/api/users/recruitable")
    Call<List<User>> getRecruitableUsers();

    @GET("/api/teams/recruiting")
    Call<List<Team>> getRecruitingTeams();

    @GET("/api/teams")
    Call<List<Team>> getTeams();

    @POST("/api/users/{id}/token/update")
    Call<String> updateUserToken(@Path("id") int id,
                                 @Body String token,
                                 @Header("Authorization") String auth,
                                 @Header("Accept") String accept);
}
