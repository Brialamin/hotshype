package com.example.sra28.hotshype.data.model;

/**
 * Created by sra28 on 7/12/2017.
 */

import android.content.SharedPreferences;
import android.util.Log;

import com.example.sra28.hotshype.data.remote.ApiUtils;
import com.example.sra28.hotshype.data.remote.HotsHypeService;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class User {

    protected static String TAG = "Hotshype-UserModel";

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("created_at")
    @Expose
    private String created_at;
    @SerializedName("updated_at")
    @Expose
    private String updated_at;
    @SerializedName("user_data")
    @Expose
    private UserData userData;
    @SerializedName("roles")
    @Expose
    private List<Role> roles = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return getName();
    }

    public static void storeUser(User user, SharedPreferences pref)
    {
        SharedPreferences.Editor prefEditor = pref.edit();

        prefEditor.putInt("id", user.getId());
        prefEditor.putString("name", user.getName());
        prefEditor.putString("email", user.getEmail());
        prefEditor.putString("created_at", user.getCreated_at());
        prefEditor.putString("updated_at", user.getUpdated_at());
        prefEditor.putString("bnet", user.getUserData().getBnetTag());
        prefEditor.putString("twitch", user.getUserData().getTwitchName());
        prefEditor.commit();

        updateTokenOnLogin(user.getId(), pref);
    }

    public static void updateNotificationToken(String token)
    {

    }

    public static void updateTokenOnLogin(int user_id, SharedPreferences settings)
    {
        HotsHypeService mService = ApiUtils.getHotsHypeService();
        mService.updateUserToken(user_id, settings.getString("notification_token", ""), "Bearer "+settings.getString("api_key", ""), "application/json").enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()) {
                    Log.d(TAG, "Updated user token");
                } else {
                    int statusCode  = response.code();
                    Log.d(TAG, "Unable to update token! Status Code: " + statusCode);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "Error communicating with the server.  Error: " + t.toString());
            }
        });
    }

    public static void removeStoredUser(SharedPreferences pref)
    {
        SharedPreferences.Editor prefEditor = pref.edit();

        prefEditor.remove("id");
        prefEditor.remove("name");
        prefEditor.remove("email");
        prefEditor.remove("created_at");
        prefEditor.remove("updated_at");
        prefEditor.remove("bnet");
        prefEditor.remove("twitch");
        prefEditor.apply();
    }

    public static void getUser(String username, final SharedPreferences settings)
    {
        HotsHypeService mService = ApiUtils.getHotsHypeService();
        try {
            mService.getUsers(URLEncoder.encode(username, "UTF-8")).enqueue(new Callback<User>() {
                public void onResponse(Call<User> call, Response<User> response) {
                    if (response.isSuccessful()) {
                        User.storeUser(response.body(), settings);
                    } else {
                        int statusCode = response.code();
                        Log.d(TAG, "Unable to login! Status Code: " + statusCode);
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    Log.d(TAG, "Error communicating with the server.  Error: " + t.toString());
                }
            });
        }
        catch(UnsupportedEncodingException e)
        {

        }
    }

    public static User getUser(int id)
    {
        final List<User> users = Arrays.asList();
        HotsHypeService mService = ApiUtils.getHotsHypeService();
        mService.getUsers(id).enqueue(new Callback<User>() {
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    users.add(response.body());
                } else {
                    int statusCode = response.code();
                    Log.d(TAG, "Unable to login! Status Code: " + statusCode);
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d(TAG, "Error communicating with the server.  Error: " + t.toString());
            }
        });
        return users.get(0);
    }

    public static int getStoredUserId(SharedPreferences pref)
    {
        return pref.getInt("id", 0);
    }

}
