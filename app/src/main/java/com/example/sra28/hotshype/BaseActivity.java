package com.example.sra28.hotshype;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.content.SharedPreferences;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.example.sra28.hotshype.data.model.User;
import com.example.sra28.hotshype.data.remote.HotsHypeService;

import java.util.ArrayList;

public class BaseActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    protected String TAG = "Hotshype";
    protected HotsHypeService mService;
    SharedPreferences settings;
    ProgressDialog progress;
    SharedPreferences.Editor prefEditor;

    final String[] _Roles = {"Tank", "Bruiser", "Healer", "Support", "Ambusher", "Burst Damage", "Sustained Damage", "Siege", "Utility"};

    private final String CLIENT_KEY = "ADQDqikReQl8xkRT4QwlaCcAF9zcmRObpjGKZAlc";
    private final String CLIENT_ID  = "3";
    private final String GRANT_TYPE = "password";
    private final String SCOPE      = "*";
    NavigationView navigationView;
    View hView;

    protected void onCreateDrawer(@LayoutRes int layoutResID) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        progress = new ProgressDialog(this);
        progress.setCancelable(false);

        settings = getSharedPreferences("HotsHypePreferences", MODE_PRIVATE);
        prefEditor = settings.edit();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        hView = navigationView.getHeaderView(0);
        Menu menu = navigationView.getMenu();

        TextView nav_user = (TextView) hView.findViewById(R.id.menu_name);
        TextView nav_email = (TextView) hView.findViewById(R.id.menu_email);

        nav_user.setText(settings.getString("name", "HotS Hype"));
        nav_email.setText(settings.getString("bnet", "hotshype@gmail.com"));

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                TextView nav_user = (TextView) hView.findViewById(R.id.menu_name);
                TextView nav_email = (TextView) hView.findViewById(R.id.menu_email);

                nav_user.setText(settings.getString("name", "HotS Hype"));
                nav_email.setText(settings.getString("bnet", "hotshype@gmail.com"));
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                Log.d(TAG, "onDrawerClosed: " + getTitle());

                invalidateOptionsMenu();
            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        MenuItem play       = menu.findItem(R.id.play);
        MenuItem recruiting = menu.findItem(R.id.recruiting);
        MenuItem matches    = menu.findItem(R.id.matches);
        MenuItem forums     = menu.findItem(R.id.forums);
        MenuItem home       = menu.findItem(R.id.home);
        MenuItem browse     = menu.findItem(R.id.browse);
        MenuItem account    = menu.findItem(R.id.account);

        SpannableString s = new SpannableString(play.getTitle());
        s.setSpan(new TextAppearanceSpan(this, R.style.ItemMenuTitleTextColor), 0, s.length(), 0);
        play.setTitle(s);

        s = new SpannableString(recruiting.getTitle());
        s.setSpan(new TextAppearanceSpan(this, R.style.ItemMenuTitleTextColor), 0, s.length(), 0);
        recruiting.setTitle(s);

        s = new SpannableString(matches.getTitle());
        s.setSpan(new TextAppearanceSpan(this, R.style.ItemMenuTitleTextColor), 0, s.length(), 0);
        matches.setTitle(s);

        s = new SpannableString(forums.getTitle());
        s.setSpan(new TextAppearanceSpan(this, R.style.ItemMenuTitleTextColor), 0, s.length(), 0);
        forums.setTitle(s);

        s = new SpannableString(home.getTitle());
        s.setSpan(new TextAppearanceSpan(this, R.style.ItemMenuTitleTextColor), 0, s.length(), 0);
        home.setTitle(s);

        s = new SpannableString(browse.getTitle());
        s.setSpan(new TextAppearanceSpan(this, R.style.ItemMenuTitleTextColor), 0, s.length(), 0);
        browse.setTitle(s);

        s = new SpannableString(account.getTitle());
        s.setSpan(new TextAppearanceSpan(this, R.style.ItemMenuTitleTextColor), 0, s.length(), 0);
        account.setTitle(s);

        ViewStub stub = (ViewStub) findViewById(R.id.include);
        stub.setLayoutResource(layoutResID);
        View inflated = stub.inflate();

    }

    @Override
    public void setContentView(@LayoutRes int layoutResID)
    {
        super.setContentView(R.layout.app_bar_main);
        onCreateDrawer(layoutResID);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        NavigationView nv= (NavigationView) findViewById(R.id.nav_view);
        Menu m=nv.getMenu();
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.home) {
            Intent i = new Intent(BaseActivity.this, MainActivity.class);
            startActivity(i);
        } else if (id == R.id.forums) {
            Log.d(TAG, "API Key: " + getApi_key());
//            Intent i = new Intent(BaseActivity.this, ForumsActivity.class);
//            startActivity(i);
        } else if (id == R.id.play) {
            boolean b=!m.findItem(R.id.league_play).isVisible();
            m.findItem(R.id.league_play).setVisible(b);
            m.findItem(R.id.friendlies_scrims).setVisible(b);
            m.findItem(R.id.rules).setVisible(b);
            return true;
        } else if (id == R.id.recruiting) {
            boolean b=!m.findItem(R.id.recruiting_team).isVisible();
            m.findItem(R.id.recruiting_team).setVisible(b);
            m.findItem(R.id.recruiting_player).setVisible(b);
            return true;
        } else if (id == R.id.matches) {
            boolean b=!m.findItem(R.id.upcomming).isVisible();
            m.findItem(R.id.upcomming).setVisible(b);
            m.findItem(R.id.live).setVisible(b);
            m.findItem(R.id.recent).setVisible(b);
            return true;
        } else if (id == R.id.browse) {
            boolean b=!m.findItem(R.id.users).isVisible();
            m.findItem(R.id.users).setVisible(b);
            m.findItem(R.id.teams).setVisible(b);
            return true;
        } else if (id == R.id.account) {
            //if a user is logged in
            if(getApi_key() != "") {
                boolean b=!m.findItem(R.id.your_teams).isVisible();
                m.findItem(R.id.login).setVisible(false);
                m.findItem(R.id.register).setVisible(false);
                m.findItem(R.id.your_teams).setVisible(b);
                m.findItem(R.id.your_account).setVisible(b);
                m.findItem(R.id.logout).setVisible(b);
            }
            else
            {
                boolean b=!m.findItem(R.id.login).isVisible();
                m.findItem(R.id.login).setVisible(b);
                m.findItem(R.id.register).setVisible(b);
                m.findItem(R.id.your_teams).setVisible(false);
                m.findItem(R.id.your_account).setVisible(false);
                m.findItem(R.id.logout).setVisible(false);
            }
            return true;
        } else if (id == R.id.users) {
            Intent i = new Intent(BaseActivity.this, DisplayUsersActivity.class);
            startActivity(i);
        } else if (id == R.id.teams) {
            Intent i = new Intent(BaseActivity.this, DisplayTeamsActivity.class);
            startActivity(i);
        } else if (id == R.id.login) {
            Intent i = new Intent(BaseActivity.this, LoginActivity.class);
            startActivity(i);
        } else if (id == R.id.register) {
            Intent i = new Intent(BaseActivity.this, RegistrationActivity.class);
            startActivity(i);
        } else if (id == R.id.logout) {
            logout();
        } else if (id == R.id.your_account) {
            Intent i = new Intent(BaseActivity.this, UserDetailsActivity.class);
            i.putExtra("user_id", User.getStoredUserId(settings));
            startActivity(i);
        } else if (id == R.id.recruiting_player) {
            Intent i = new Intent(BaseActivity.this, PlayersLookingForTeamActivity.class);
            startActivity(i);
        } else if (id == R.id.recruiting_team) {
            Intent i = new Intent(BaseActivity.this, TeamsLookingForPlayersActivity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        refreshMenu();
        return true;
    }

    public String getClient_key() {
        return CLIENT_KEY;
    }

    public String getClient_id() {
        return CLIENT_ID;
    }

    public String getGrant_type() {
        return GRANT_TYPE;
    }

    public String getScope() {
        return SCOPE;
    }

    public String getApi_key() {
        return settings.getString("api_key", "");
    }

    public String getApi_key_r() {
        return settings.getString("api_key_r", "");
    }

    public void setApi_key_r(String api_key_r) {
        prefEditor.putString("api_key_r", api_key_r); prefEditor.commit();
    }

    public void setApi_key(String api_key) {
        prefEditor.putString("api_key", api_key); prefEditor.commit();
    }

    public void refreshMenu()
    {
        //get nav view and menu
        NavigationView nv= (NavigationView) findViewById(R.id.nav_view);
        Menu m=nv.getMenu();

        //reset the sub-views to all be invisible
        m.findItem(R.id.login).setVisible(false);
        m.findItem(R.id.register).setVisible(false);
        m.findItem(R.id.your_teams).setVisible(false);
        m.findItem(R.id.your_account).setVisible(false);
        m.findItem(R.id.logout).setVisible(false);
        m.findItem(R.id.users).setVisible(false);
        m.findItem(R.id.teams).setVisible(false);
        m.findItem(R.id.upcomming).setVisible(false);
        m.findItem(R.id.live).setVisible(false);
        m.findItem(R.id.recent).setVisible(false);
        m.findItem(R.id.recruiting_team).setVisible(false);
        m.findItem(R.id.recruiting_player).setVisible(false);
        m.findItem(R.id.league_play).setVisible(false);
        m.findItem(R.id.friendlies_scrims).setVisible(false);
        m.findItem(R.id.rules).setVisible(false);

        //get header view and re-check the headers to make sure user didn't log out
//        TextView nav_user = (TextView) hView.findViewById(R.id.menu_name);
//        TextView nav_email = (TextView) hView.findViewById(R.id.menu_email);
//        nav_user.setText(settings.getString("name", "HotS Hype"));
//        nav_email.setText(settings.getString("bnet", "hotshype@gmail.com"));

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void logout()
    {
        this.setApi_key("");
        this.setApi_key_r("");
        User.removeStoredUser(settings);
        refreshMenu();
        Intent i = new Intent(BaseActivity.this, MainActivity.class);
        startActivity(i);

        TextView nav_user = (TextView) hView.findViewById(R.id.menu_name);
        TextView nav_email = (TextView) hView.findViewById(R.id.menu_email);
        nav_user.setText(settings.getString("name", "HotS Hype"));
        nav_email.setText(settings.getString("bnet", "hotshype@gmail.com"));
    }

    public class CustomDarkSpinner extends BaseAdapter implements SpinnerAdapter
    {
        private final Context activity;
        private ArrayList<String> asr;

        public CustomDarkSpinner(Context context, ArrayList<String> asr)
        {
            this.asr = asr;
            activity = context;
        }

        public int getCount()
        {
            return asr.size();
        }
        public Object getItem(int i)
        {
            return asr.get(i);
        }
        public long getItemId(int i)
        {
            return (long)i;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent)
        {
            TextView txt = new TextView(BaseActivity.this);
            txt.setPadding(16, 16, 16, 16);
            txt.setTextSize(20);
            txt.setBackgroundColor(getResources().getColor(R.color.dark_bg_accent));
            txt.setTextColor(getResources().getColor(R.color.inv_text));
            txt.setGravity(Gravity.CENTER);
            txt.setText(asr.get(position));

            return txt;

        }

        public View getView(int i, View view, ViewGroup viewGroup)
        {
            TextView txt = new TextView(BaseActivity.this);
            txt.setPadding(16, 16, 16, 16);
            txt.setTextSize(20);
            txt.setGravity(Gravity.CENTER);
            txt.setTextColor(getResources().getColor(R.color.inv_text));
            txt.setGravity(Gravity.CENTER);
            txt.setText(asr.get(i));

            return txt;
        }

    }
}
