package com.example.sra28.hotshype;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.sra28.hotshype.data.model.Team;
import com.example.sra28.hotshype.data.remote.ApiUtils;
import com.example.sra28.hotshype.drawingObjects.TeamAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DisplayTeamsActivity extends BaseActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_display_teams);
        TAG = TAG + "-DisplayTeamsActivity";
        mService = ApiUtils.getHotsHypeService();

        progress.setMessage("Loading teams...");

        loadTeams();
    }

    public void loadTeams()
    {
        progress.show();
        mService.getTeams().enqueue(new Callback<List<Team>>() {
            @Override
            public void onResponse(Call<List<Team>> call, Response<List<Team>> response) {
                if(response.isSuccessful()) {
                    Log.d(TAG, "teams loaded from API");
                    populateTeamList(response.body());
                }else {
                    progress.dismiss();
                    int statusCode  = response.code();
                    Log.d(TAG, "Unable to load teams! Status Code: " + statusCode);
                    Toast.makeText(getApplicationContext(), "Error loading teams", Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onFailure(Call<List<Team>> call, Throwable t) {
                progress.dismiss();
                Log.d(TAG, "Error communicating with the server.  Error: " + t.toString());
                Toast.makeText(getApplicationContext(), "Error communicating with the server", Toast.LENGTH_SHORT);
            }
        });
    }

    public void populateTeamList(List<Team> teams)
    {
        final ListView teamView = (ListView) findViewById(R.id.team_list);

        TeamAdapter mAdapter = new TeamAdapter(this, R.layout.list_view_text, teams);

        teamView.setAdapter(mAdapter);

        teamView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int team_id = ((Team) teamView.getAdapter().getItem(position)).getId();
                Intent i = new Intent(DisplayTeamsActivity.this, UserDetailsActivity.class);
                i.putExtra("team_id", team_id);
                startActivity(i);
            }
        });

        progress.dismiss();
    }
}
