package com.example.sra28.hotshype;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.sra28.hotshype.data.model.ApiTokenRequest;
import com.example.sra28.hotshype.data.model.User;
import com.example.sra28.hotshype.data.remote.ApiUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    EditText email, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_login);

        TAG = TAG + "-LoginActivity";
        mService = ApiUtils.getHotsHypeService();
        progress.setMessage("Logging in...");
    }

    public void login(View v)
    {

        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);

        progress.show();

        mService.login(getGrant_type(), getClient_id(), getClient_key(), email.getText().toString(), password.getText().toString(), getScope()).enqueue(new Callback<ApiTokenRequest>() {
            @Override
            public void onResponse(Call<ApiTokenRequest> call, Response<ApiTokenRequest> response) {
                if(response.isSuccessful()) {
                    progress.dismiss();
                    Log.d(TAG, "login sent successfully");
                    TextView user = (TextView) findViewById(R.id.user);
                    user.setText(response.body().toString());
                    setApi_key(response.body().getAccessToken());
                    setApi_key_r(response.body().getRefreshToken());
                    User.getUser(email.getText().toString(), settings);

                    TextView nav_user = (TextView) hView.findViewById(R.id.menu_name);
                    TextView nav_email = (TextView) hView.findViewById(R.id.menu_email);

                    nav_user.setText(settings.getString("name", "HotS Hype"));
                    nav_email.setText(settings.getString("bnet", "hotshype@gmail.com"));

                    finish();
                }else {
                    progress.dismiss();
                    int statusCode  = response.code();
                    Log.d(TAG, "Unable to login! Status Code: " + statusCode);
                    TextView user = (TextView) findViewById(R.id.user);
                    user.setText("Unable to log in, please check your credentials and try again!");
                }
            }

            @Override
            public void onFailure(Call<ApiTokenRequest> call, Throwable t) {
                progress.dismiss();
                Log.d(TAG, "Error communicating with the server.  Error: " + t.toString());
            }
        });
    }



}
