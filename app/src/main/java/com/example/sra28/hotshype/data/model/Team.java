package com.example.sra28.hotshype.data.model;

/**
 * Created by sra28 on 7/18/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Team {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("logo_location")
    @Expose
    private String logoLocation;
    @SerializedName("recruiting")
    @Expose
    private Integer recruiting;
    @SerializedName("captain")
    @Expose
    private Integer captain;
    @SerializedName("players")
    @Expose
    private List<User> players;
    @SerializedName("team_captain")
    @Expose
    private User teamCaptain;
    @SerializedName("looking_for_roles")
    @Expose
    private List<Role> lookingForRoles = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogoLocation() {
        return logoLocation;
    }

    public void setLogoLocation(String logoLocation) {
        this.logoLocation = logoLocation;
    }

    public Integer getRecruiting() {
        return recruiting;
    }

    public void setRecruiting(Integer recruiting) {
        this.recruiting = recruiting;
    }

    public Integer getCaptain() {
        return captain;
    }

    public void setCaptain(Integer captain) {
        this.captain = captain;
    }

    public User getTeamCaptain() {
        return teamCaptain;
    }

    public void setTeamCaptain(User teamCaptain) {
        this.teamCaptain = teamCaptain;
    }

    public List<Role> getRoles() {
        return lookingForRoles;
    }

    public void setRoles(List<Role> roles) {
        this.lookingForRoles = roles;
    }

    public List<User> getPlayers() {
        return players;
    }

    public void setPlayers(List<User> players) {
        this.players = players;
    }
}
