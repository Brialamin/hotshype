package com.example.sra28.hotshype.drawingObjects;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.sra28.hotshype.R;
import com.example.sra28.hotshype.data.model.Conversation;
import com.example.sra28.hotshype.data.model.Message;
import com.example.sra28.hotshype.data.model.User;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by sra28 on 8/7/2017.
 */

public class ConversationAdapter extends ArrayAdapter<Conversation> {
    private final String TAG = "ConversationAdapter";
    SharedPreferences settings;

    public ConversationAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public ConversationAdapter(Context context, int resource, List<Conversation> items) {
        super(context, resource, items);
        settings = context.getSharedPreferences("HotsHypePreferences", MODE_PRIVATE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.item_list_row, null);
        }

        Conversation p = getItem(position);
        List<User> users = p.getUsers();
        int logged_in_user_id = settings.getInt("id",0);
        User notLoggedIn = new User();
        for(User user : users)
        {
            if(user.getId() != logged_in_user_id)
                notLoggedIn = user;
        }

        if (p != null) {
            TextView tt1 = (TextView) v.findViewById(R.id.name);
            TextView tt2 = (TextView) v.findViewById(R.id.message);

            if (tt1 != null) {
                tt1.setText(notLoggedIn.getName());
            }

            if (tt2 != null) {
                tt2.setText(p.getLastMessage().getFrom().getName() + ": " + p.getLastMessage().getMessage());
            }
        }

        return v;
    }
}
