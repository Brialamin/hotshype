package com.example.sra28.hotshype.data.remote;

import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sra28 on 8/25/2017.
 */

public class FirebaseIDService extends FirebaseInstanceIdService {
    private static final String TAG = "FirebaseIDService";
    protected final HotsHypeService mService = ApiUtils.getHotsHypeService();
    private int tokenDelay = 5000; //1 second delay for retrieving messages
    SharedPreferences settings;
    SharedPreferences.Editor prefEditor;

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        settings = getSharedPreferences("HotsHypePreferences", MODE_PRIVATE);
        prefEditor = settings.edit();

        prefEditor.putString("notification_token", refreshedToken); prefEditor.commit();

        // TODO: Implement this method to send any registration to your app's servers.
        sendRegistrationToServer(refreshedToken);
    }

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        int user_id = settings.getInt("id", 0);

        if(user_id != 0)
        {
            mService.updateUserToken(user_id, token, "Bearer "+settings.getString("api_key", ""), "application/json").enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if(response.isSuccessful()) {
                        Log.d(TAG, "Updated user token");
                    } else {
                        int statusCode  = response.code();
                        Log.d(TAG, "Unable to update token! Status Code: " + statusCode);
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.d(TAG, "Error communicating with the server.  Error: " + t.toString());
                }
            });
        }
    }
}
