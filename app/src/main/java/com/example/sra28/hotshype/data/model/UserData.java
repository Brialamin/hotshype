package com.example.sra28.hotshype.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserData {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("last_active")
    @Expose
    private Object lastActive;
    @SerializedName("twitch_name")
    @Expose
    private String twitchName;
    @SerializedName("bnet_tag")
    @Expose
    private String bnetTag;
    @SerializedName("posts")
    @Expose
    private Integer posts;
    @SerializedName("is_streamer")
    @Expose
    private Integer isStreamer;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("recruitable")
    @Expose
    private Integer recruitable;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Object getLastActive() {
        return lastActive;
    }

    public void setLastActive(Object lastActive) {
        this.lastActive = lastActive;
    }

    public String getTwitchName() {
        return twitchName;
    }

    public void setTwitchName(String twitchName) {
        this.twitchName = twitchName;
    }

    public String getBnetTag() {
        return bnetTag;
    }

    public void setBnetTag(String bnetTag) {
        this.bnetTag = bnetTag;
    }

    public Integer getPosts() {
        return posts;
    }

    public void setPosts(Integer posts) {
        this.posts = posts;
    }

    public Integer getIsStreamer() {
        return isStreamer;
    }

    public void setIsStreamer(Integer isStreamer) {
        this.isStreamer = isStreamer;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public Integer getRecruitable() {
        return recruitable;
    }

    public void setRecruitable(Integer recruitable) {
        this.recruitable = recruitable;
    }

}