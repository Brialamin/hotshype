package com.example.sra28.hotshype.drawingObjects;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.sra28.hotshype.R;
import com.example.sra28.hotshype.data.model.Role;
import com.example.sra28.hotshype.data.model.User;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class UserAdapter extends ArrayAdapter<User> {

    final String[] _Roles = {"Tank", "Bruiser", "Healer", "Support", "Ambusher", "Burst Damage", "Sustained Damage", "Siege", "Utility"};
    private final String TAG = "UserAdapter";
    SharedPreferences settings;

    public UserAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public UserAdapter(Context context, int resource, List<User> items) {
        super(context, resource, items);
        settings = context.getSharedPreferences("HotsHypePreferences", MODE_PRIVATE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.item_list_row, null);
        }

        User p = getItem(position);
        List<Role> roles = p.getRoles();
        String roleString = "Roles:";

        for(Role role : roles)
        {
            if(role.getSelected() == 1)
            {
                roleString += " " + _Roles[role.getRoleId()-1] + " -";
            }
        }

        roleString = roleString.substring(0, roleString.length() - 2);

        if (p != null) {
            TextView tt1 = (TextView) v.findViewById(R.id.name);
            TextView tt2 = (TextView) v.findViewById(R.id.message);

            if (tt1 != null) {
                tt1.setText(p.getName());
            }

            if (tt2 != null) {
                tt2.setText(roleString);
            }
        }

        return v;
    }

}