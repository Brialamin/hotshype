package com.example.sra28.hotshype.drawingObjects;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.sra28.hotshype.R;
import com.example.sra28.hotshype.data.model.Message;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class MessageAdapter extends ArrayAdapter<Message> {

    private final String TAG = "MessageAdapter";
    SharedPreferences settings;
    SharedPreferences.Editor prefEditor;

    public MessageAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public MessageAdapter(Context context, int resource, List<Message> items) {
        super(context, resource, items);
        settings = context.getSharedPreferences("HotsHypePreferences", MODE_PRIVATE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.item_list_row, null);
        }

        Message p = getItem(position);

        if (p != null) {
            TextView tt1 = (TextView) v.findViewById(R.id.name);
            TextView tt2 = (TextView) v.findViewById(R.id.message);

            //trying to set the top row to the right side of the screen if the message is sent from the current user, but it's having issues right now
            //this will be worked on later
//            TableRow tr1 = (TableRow) v.findViewById(R.id.TableRow01);
//
//            int logged_in_user_id = settings.getInt("id",0);
//
//            Log.v(TAG, "User ID: " + logged_in_user_id);
//            Log.v(TAG, "From ID: " + p.getFrom().getId());
//
//            if(p.getFrom().getId() == logged_in_user_id)
//            {
//                tt1.setGravity(Gravity.RIGHT);
//                tr1.setGravity(Gravity.RIGHT);
//            }

            if (tt1 != null) {
                tt1.setText(p.getFrom().getName());
            }

            if (tt2 != null) {
                tt2.setText(p.getMessage());
            }
        }

        return v;
    }

}