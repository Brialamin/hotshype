package com.example.sra28.hotshype.data.model;

import android.util.Log;

import com.example.sra28.hotshype.data.remote.HotsHypeService;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Conversation {

    private final String TAG = "ConversationModel";

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("name")
    @Expose
    private String name;
    private List<User> users;
    private Message lastMessage;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void getUsers(HotsHypeService mService)
    {
        mService.getConversationUsers(this.id).enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if(response.isSuccessful()) {
                    Log.d(TAG, "retrieved users");
                    setUsers(response.body());
                }else {
                    int statusCode  = response.code();
                    Log.d(TAG, "Unable to get users! Status Code: " + statusCode);
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Log.d(TAG, "Error communicating with the server getting users.  Error: " + t.toString());
            }
        });
    }

    public void getLastMessage(HotsHypeService mService)
    {
        mService.getConversationLastMessage(this.id).enqueue(new Callback<Message>() {
            @Override
            public void onResponse(Call<Message> call, Response<Message> response) {
                if(response.isSuccessful()) {
                    Log.d(TAG, "retrieved last message");
                    setLastMessage(response.body());
                }else {
                    int statusCode  = response.code();
                    Log.d(TAG, "Unable to get last message! Status Code: " + statusCode);
                }
            }

            @Override
            public void onFailure(Call<Message> call, Throwable t) {
                Log.d(TAG, "Error communicating with the server getting last message.  Error: " + t.toString());
            }
        });
    }

    public void setUsers(List<User> users)
    {
        this.users = users;
    }

    public List<User> getUsers()
    {
        return users;
    }

    public void setLastMessage(Message message)
    {
        this.lastMessage = message;
    }

    public Message getLastMessage()
    {
        return lastMessage;
    }

}