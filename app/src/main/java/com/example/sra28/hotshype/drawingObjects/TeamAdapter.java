package com.example.sra28.hotshype.drawingObjects;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sra28.hotshype.R;
import com.example.sra28.hotshype.data.model.Role;
import com.example.sra28.hotshype.data.model.Team;
import com.example.sra28.hotshype.data.model.User;

import java.io.InputStream;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by sra28 on 8/10/2017.
 */

public class TeamAdapter extends ArrayAdapter<Team> {

    private final String[] _Roles = {"Tank", "Bruiser", "Healer", "Support", "Ambusher", "Burst Damage", "Sustained Damage", "Siege", "Utility"};
    private final String TAG = "UserAdapter";
    private final String BASE_LOGO_URL = "http://hotshype.duckdns.org/storage/team_logos/";
    private boolean recruitmentPage = false;
    SharedPreferences settings;

    public TeamAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public TeamAdapter(Context context, int resource, List<Team> items) {
        super(context, resource, items);
        settings = context.getSharedPreferences("HotsHypePreferences", MODE_PRIVATE);
    }

    public TeamAdapter(Context context, int resource, List<Team> items, boolean recruitmentPage) {
        super(context, resource, items);
        settings = context.getSharedPreferences("HotsHypePreferences", MODE_PRIVATE);
        this.recruitmentPage = recruitmentPage;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.item_list_row_with_image, null);
        }

        Team p = getItem(position);
        List<Role> roles = p.getRoles();
        String roleString = "Roles:";
        String teamCaptainString = "Captain: "+p.getTeamCaptain().getName();
        String secondLine = teamCaptainString;

        for(Role role : roles)
        {
            if(role.getSelected() == 1)
            {
                roleString += " " + _Roles[role.getRoleId()-1] + " -";
            }
        }

        roleString = roleString.substring(0, roleString.length() - 2);

        if(recruitmentPage)
            secondLine += "\n"+roleString;

        if (p != null) {
            ImageView iv1 = v.findViewById(R.id.logo);
            TextView tt1  = v.findViewById(R.id.name);
            TextView tt2  = v.findViewById(R.id.message);

            if (tt1 != null) {
                tt1.setText(p.getName());
            }

            if (tt2 != null) {
                tt2.setText(secondLine);
            }

            if (iv1 != null) {
                new DownloadImageTask(iv1).execute(BASE_LOGO_URL + p.getLogoLocation());
            }
        }

        return v;
    }

    private static class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}


