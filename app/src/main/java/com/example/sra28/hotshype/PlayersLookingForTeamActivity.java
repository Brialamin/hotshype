package com.example.sra28.hotshype;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.sra28.hotshype.data.model.Conversation;
import com.example.sra28.hotshype.data.model.User;
import com.example.sra28.hotshype.data.remote.ApiUtils;
import com.example.sra28.hotshype.drawingObjects.ConversationAdapter;
import com.example.sra28.hotshype.drawingObjects.UserAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sra28 on 8/7/2017.
 */

public class PlayersLookingForTeamActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_players_looking_for_team);
        TAG = TAG + "-PlayersLookingForTeamActivity";

        mService = ApiUtils.getHotsHypeService();

        progress.setMessage("Loading players...");

        loadUsers();
    }

    public void loadUsers()
    {
        progress.show();
        mService.getRecruitableUsers().enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if(response.isSuccessful()) {
                    Log.d(TAG, "users loaded from API");
                    populateUserList(response.body());
                }else {
                    progress.dismiss();
                    int statusCode  = response.code();
                    Log.d(TAG, "Unable to load users! Status Code: " + statusCode);
                    Toast.makeText(getApplicationContext(), "Error loading users", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                progress.dismiss();
                Log.d(TAG, "Error communicating with the server.  Error: " + t.toString());
                Toast.makeText(getApplicationContext(), "Error communicating with the server", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void populateUserList(List<User> users)
    {
        final ListView userView = (ListView) findViewById(R.id.looking_for_team_view);

        UserAdapter mAdapter = new UserAdapter(this, R.layout.list_view_text, users);

        userView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int user_id = ((User) userView.getAdapter().getItem(position)).getId();
                Intent i = new Intent(PlayersLookingForTeamActivity.this, UserDetailsActivity.class);
                i.putExtra("user_id", user_id);
                startActivity(i);
            }
        });

        userView.setAdapter(mAdapter);

        progress.dismiss();
    }
}
