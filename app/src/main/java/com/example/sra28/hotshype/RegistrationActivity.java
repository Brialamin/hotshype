package com.example.sra28.hotshype;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.sra28.hotshype.data.model.ApiTokenRequest;
import com.example.sra28.hotshype.data.model.User;
import com.example.sra28.hotshype.data.remote.ApiUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends BaseActivity {

    EditText username, password, confirmPassword, bnet, twitch, email;
    TextView passwordError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_registration);

        TAG = TAG + "-RegistrationActivity";
        mService = ApiUtils.getHotsHypeService();

        username        = (EditText) findViewById(R.id.name);
        password        = (EditText) findViewById(R.id.password);
        confirmPassword = (EditText) findViewById(R.id.confirm_password);
        email           = (EditText) findViewById(R.id.email);
        bnet            = (EditText) findViewById(R.id.bnet_tag);
        twitch          = (EditText) findViewById(R.id.twitch_name);
    }

    public void register(View v)
    {

        progress.setMessage("Registering account...");

        String usernameString        = username.getText().toString();
        String passwordString        = password.getText().toString();
        String confirmPasswordString = confirmPassword.getText().toString();
        String emailString           = email.getText().toString();
        String bnetString            = bnet.getText().toString();
        String twitchString          = twitch.getText().toString();

        passwordError   = (TextView) findViewById(R.id.mismatch_passwords);

        progress.show();

        if(confirmPasswordString.equals(passwordString)) {
            mService.register(bnetString, twitchString, usernameString, emailString, passwordString).enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    if(response.isSuccessful()) {
                        progress.dismiss();
                        Log.d(TAG, "registered user successfully");
                        login();
                    }else {
                        progress.dismiss();
                        int statusCode  = response.code();
                        Log.d(TAG, "Unable to register! Status Code: " + statusCode);
                        TextView error = (TextView) findViewById(R.id.error_message);
                        error.setText("Unable to register!");
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    Log.d(TAG, "Error communicating with the server.  Error: " + t.toString());
                }
            });
        } else {
            progress.dismiss();
            Log.d(TAG, "Password: " + passwordString);
            Log.d(TAG, "Confirm Password: " + confirmPasswordString);
            passwordError.setVisibility(View.VISIBLE);
        }
    }

    public void login()
    {
        progress.setMessage("Logging In...");

        progress.show();
        mService.login(getGrant_type(), getClient_id(), getClient_key(), email.getText().toString(), password.getText().toString(), getScope()).enqueue(new Callback<ApiTokenRequest>() {
            @Override
            public void onResponse(Call<ApiTokenRequest> call, Response<ApiTokenRequest> response) {
                if(response.isSuccessful()) {
                    progress.dismiss();
                    Log.d(TAG, "login sent successfully");
                    setApi_key(response.body().getAccessToken());
                    setApi_key_r(response.body().getRefreshToken());
                    User.getUser(email.getText().toString(), settings);
                    finish();
                }else {
                    progress.dismiss();
                    int statusCode  = response.code();
                    Log.d(TAG, "Unable to login! Status Code: " + statusCode);
                }
            }

            @Override
            public void onFailure(Call<ApiTokenRequest> call, Throwable t) {
                progress.dismiss();
                Log.d(TAG, "Error communicating with the server.  Error: " + t.toString());
            }
        });
    }
}
