package com.example.sra28.hotshype;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.sra28.hotshype.data.model.Message;
import com.example.sra28.hotshype.data.remote.ApiUtils;
import com.example.sra28.hotshype.drawingObjects.MessageAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sra28 on 8/6/2017.
 */

public class ConversationDetailsActivity extends BaseActivity {

    private int convoId, mUserId;

    private int messageDelay = 1000; //1 second delay for retrieving messages

    private Handler messenger;

    MessageAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_conversation_details);
        mService = ApiUtils.getHotsHypeService();
        Intent i = getIntent();
        Bundle b = getIntent().getExtras();
        convoId = i.getIntExtra("conversation_id", 0);
        if(convoId == 0)
        {
            convoId = Integer.parseInt(b.getString("convo_id"));
        }

        mUserId = settings.getInt("id", 0);
        Button sendMessage = (Button) findViewById(R.id.sendMessage);

        messenger = new Handler();

        getConversationMessages(convoId, true);

        messenger.post(retrieveMessages);

        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText myMessage = (EditText) findViewById(R.id.newMessage);
                String message = myMessage.getText().toString();

                Message newMessage = new Message();

                newMessage.setConversationsId(convoId);
                newMessage.setFromId(mUserId);
                newMessage.setMessage(message);

                progress.setMessage("Sending message...");
                progress.show();

                mService.createMessage(newMessage, "Bearer "+getApi_key(), "application/json").enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if(response.isSuccessful()) {
                            progress.dismiss();
                            Log.d(TAG, "message sent");
                            myMessage.setText("");
                            getConversationMessages(convoId, false);
                        }else {
                            progress.dismiss();
                            int statusCode = response.code();
                            Log.d(TAG, "Unable to send message! Status Code: " + statusCode);
                            if(statusCode == 401)
                            {
                                Toast.makeText(getApplicationContext(),"Error authenticating, please login again", Toast.LENGTH_LONG).show();
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        progress.dismiss();
                        Log.d(TAG, "Error communicating with the server.  Error: " + t.toString());
                    }
                });
            }
        });
    }

    protected void onDestroy() {
        super.onDestroy();
        messenger.removeCallbacks(retrieveMessages);
    }

    private final Runnable retrieveMessages = new Runnable() {
        @Override
        public void run() {
            getConversationMessages(convoId, false);
            messenger.postDelayed(this, messageDelay);
        }
    };

    private void getConversationMessages(int convo_id, final boolean first_time)
    {
        if(first_time) {
            progress.setMessage("Loading messages...");
            progress.show();
        }

        mService.getConversationMessages(convo_id, "Bearer "+getApi_key(), "application/json").enqueue(new Callback<List<Message>>() {
            @Override
            public void onResponse(Call<List<Message>> call, Response<List<Message>> response) {
                if(response.isSuccessful()) {
                    Log.d(TAG, "messages loaded");
                    createMessageView(response.body(), first_time);
                } else {
                    if(first_time)
                        progress.dismiss();
                    int statusCode  = response.code();
                    Log.d(TAG, "Unable to load messages! Status Code: " + statusCode);
                }
            }

            @Override
            public void onFailure(Call<List<Message>> call, Throwable t) {
                if(first_time)
                    progress.dismiss();
                Log.d(TAG, "Error communicating with the server.  Error: " + t.toString());
            }
        });
    }

    private void createMessageView(List<Message> messages, boolean first_time)
    {
        ListView convoView = (ListView) findViewById(R.id.convo_messages);

        if(mAdapter == null) {
            mAdapter = new MessageAdapter(this, R.layout.list_view_text, messages);

            convoView.setAdapter(mAdapter);

            convoView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                }
            });

            progress.dismiss();
        } else {
            mAdapter.setNotifyOnChange(false);
            mAdapter.clear();
            mAdapter.addAll(messages);
            mAdapter.notifyDataSetChanged();
        }

    }

}
