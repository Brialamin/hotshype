# README #

### What is this repository for? ###

This repo is for the application made by Samuel Adams for HotS Hype.  The goal of this application is to be used in place of the website to manage a Heroes of the Storm team for a league.

This is still a work in progress and should be considered very early alpha.

### How do I get set up? ###

The setup should already be done, dependencies are added through gradle.

### Who do I talk to? ###

Email sra2893eq@gmail.com for more information.